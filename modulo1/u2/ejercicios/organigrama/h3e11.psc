Algoritmo sin_titulo
	Leer n1
	Leer n2
	Leer n3
	Si n1>n2 && n1>n3 Entonces
		Si n2 > n3 Entonces
			Escribir "Descendente: ",n1,n2,n3
			Escribir "Ascendente: ",n3,n2,n1
		SiNo
			Escribir "Descendente: ",n1,n3,n2
			Escribir "Ascendente: ",n2,n3,n1
		FinSi
	SiNo
		Si n2 > n1 && n2 > n3 Entonces
			Si n3 > n1 Entonces
				Escribir "Descendente: ",n2,n3,n1
				Escribir "Ascendente: ",n1,n3,n2
			SiNo
				Escribir "Descendente: ",n2,n1,n3
				Escribir "Ascendente: ",n3,n1,n2
			FinSi
		SiNo
			Escribir "Descendente: ",n3,n2,n1
			Escribir "Ascendente: ",n1,n2,n3
		FinSi
	FinSi
FinAlgoritmo

