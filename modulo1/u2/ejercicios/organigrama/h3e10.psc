Algoritmo sin_titulo
	Leer nota
	Si nota >=0 && nota<= 5 Entonces
		Escribir "Suspenso"
	SiNo
		Si nota>=5 && nota <= 6 Entonces
			Escribir "Suficiente"
		SiNo
			Si nota>= 6 && nota <=7 Entonces
				Escribir "Bien"
			SiNo
				Si nota>=7 && nota <= 9 Entonces
					Escribir "Notable"
				SiNo
					Escribir "Sobresaliente"
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo

